/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jmxserver;

/**
 *
 * @author drochelet
 */
public class Hello implements HelloMBean {
   private String message = null;

   public Hello() {
      message = "Hello, world";
   }

   public Hello(String message) {
      this.message = message;
   }

   public void setMessage(String message) {
      this.message = message;
   }

   public String getMessage() {
      return message;
   }

   public void sayHello() {
      System.out.println(message);
   }
}
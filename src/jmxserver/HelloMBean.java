/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jmxserver;

/**
 *
 * @author drochelet
 */
public interface HelloMBean {
   public void setMessage(String message);
   public String getMessage();
   public void sayHello();
}